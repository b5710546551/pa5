import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;


public class ClockUI extends JFrame{
	public static JLabel hour= new JLabel("00"), min= new JLabel("00"), sec= new JLabel("00");
	private JButton set,plus,minus;
	private  int delay = 500; // milliseconds
	private static Clock clock = new Clock();
	private int setCount=0;

	public ClockUI() {
		super("Cheap Clock");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
		this.pack();
		this.setVisible(true);
	}
	public void initComponents(){
		Container container = new Container();
		super.setContentPane(container);
		container.setLayout(new GridLayout(2,1));
		container.setBackground(Color.BLACK);
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();

		Font f = new Font("Tahoma", 0, 30);
		Font d = new Font("Tahoma", Font.BOLD, 15);
		hour.setFont(f);
		min.setFont(f);
		sec.setFont(f);
		JLabel sp1 = new JLabel(" : ");
		JLabel sp2 = new JLabel(" : ");
		set = new JButton("SET");
		plus = new JButton(" + ");
		minus = new JButton(" - ");
		set.setFont(d);
		plus.setFont(d);
		minus.setFont(d);
		hour.setForeground(Color.YELLOW);
		min.setForeground(Color.YELLOW);
		sec.setForeground(Color.YELLOW);
		sp1.setForeground(Color.YELLOW);
		sp2.setForeground(Color.YELLOW);
		set.setForeground(Color.YELLOW);
		plus.setForeground(Color.YELLOW);
		minus.setForeground(Color.YELLOW);
		panel1.setLayout(new FlowLayout());
		panel2.setLayout(new FlowLayout());
		panel1.add(hour);
		panel1.add(sp1);
		panel1.add(min);
		panel1.add(sp2);
		panel1.add(sec);
		panel2.add(set);
		panel2.add(plus);
		panel2.add(minus);
		container.add(panel1);
		container.add(panel2);
		panel1.setBackground(null);
		panel2.setBackground(null);
		set.setBackground(null);
		plus.setBackground(null);
		minus.setBackground(null);
		set.addActionListener(new setButtonListener());
		plus.addActionListener(new plusButtonListener());
		minus.addActionListener(new minusButtonListener());
	}
	class setButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			clock.getState().performSet();
			setCount++;
		}
	}
	class plusButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(clock.getState() instanceof SetAlarmState){
				((SetAlarmState) clock.getState()).plusButtonClick(setCount,hour,min,sec);
			}
		}

	}

	class minusButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(clock.getState() instanceof SetAlarmState){
				((SetAlarmState) clock.getState()).minusButtonClick(setCount,hour,min,sec);
			}
		}

	}

	
	static ActionListener task = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			clock.updateTime();
			hour.setText(String.format("%02d", clock.hours));
			min.setText(String.format("%02d", clock.mins));
			sec.setText(String.format("%02d", clock.secs));
		}
	};


	public static void main(String [] args){
		DisplayTimeState state = new DisplayTimeState(clock);
		clock.setState(state);
		ClockUI clockUI = new ClockUI();
		//		Timer timer = new Timer(delay,task);
		//		timer.start();
	}
}
