

public class Clock {
	private ClockState state;
	/** delegate behavior to state objects */

	public int hours=0,mins=0,secs=0;
	
	public Clock(){
		state = new DisplayTimeState(this);
	}
	public void updateTime( ) { 
		state.updateTime(); 
	}
	public void handleSetKey() { state.performSet(); }
	public void setState( ClockState alarmState ){
		state = alarmState;
	} 
	public ClockState getState(){ return state;}
	
	
}
