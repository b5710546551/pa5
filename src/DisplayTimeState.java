import java.util.Date;



public class DisplayTimeState extends ClockState{

	private ClockTask task;
	public DisplayTimeState(Clock clock) {
		super(clock);
		task = new ClockTask(clock);
		task.run();
	}
	public void performSet( ) {
		clock.setState( new SetAlarmState(clock) );
	}
	
	private Date time = new Date();
	
	public void updateTime( ) {
	time.setTime( System.currentTimeMillis() );
		/* update the ui */
		clock.hours = time.getHours();
		clock.mins = time.getMinutes();
		clock.secs = time.getSeconds();

	}
	
}
