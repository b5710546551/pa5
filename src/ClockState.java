public abstract class ClockState {
	protected Clock clock; 
	public ClockState(Clock clock) { this.clock = clock; }
	/** handle "set" key press */
	public abstract void performSet();
	/** handle updateTime event notification */
	public abstract void updateTime();
	/** something to do before exiting this state */
	public void leaveState( ) { };
	//TODO What other methods depend on state?
}
