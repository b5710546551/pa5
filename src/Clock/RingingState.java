package Clock;
/**
 * RingingState that will ring when time is equal to time that was set from CheapCLock
 * @author Chinatip Vichian
 *
 */
public class RingingState extends ClockState{

	public RingingState(Clock clock) {
		super(clock);
	}
	/** handle "set" key press */
	@Override
	public void performSet() {
		clock.state = new DisplayTimeState(clock);
		clock.alarmOn = false;
		CheapClock.clip.stop();
	}
	/** handle updateTime event notification */
	@Override
	public void updateTime() {
		clock.hour = clock.ringHour+0;
		clock.min = clock.ringMin+0;
		clock.sec = clock.ringSec+0;
		
	}
	/** Call this method when plus button is pressed **/
	@Override
	public void plus() {
		clock.state = new DisplayTimeState(clock);
		clock.alarmOn = false;
		CheapClock.clip.stop();
	}
	/** Call this method when minus button is pressed **/
	@Override
	public void minus() {
		clock.state = new DisplayTimeState(clock);
		clock.alarmOn = false;
		CheapClock.clip.stop();
	}
	

}
