package Clock;
/**
 * DisplayTimeState is state that will show time that updates every second
 * @author Chinatip Vichian
 *
 */
public class AlarmTimeState extends ClockState{

	public AlarmTimeState(Clock clock) {
		super(clock);
		// TODO Auto-generated constructor stub
	}
	/** handle "set" key press */
	@Override
	public void performSet() {
		// TODO Auto-generated method stub
		
	}
	/** handle updateTime event notification */
	@Override
	public void updateTime() {
		clock.hour = clock.ringHour;
		clock.min = clock.ringMin;
		clock.sec = clock.ringSec;
	}
	/** Call this method when plus button is pressed **/
	@Override
	public void plus() {
		clock.state = new DisplayTimeState(clock);
	}
	/** Call this method when minus button is pressed **/
	@Override
	public void minus() {
		// TODO Auto-generated method stub
		
	}

}
