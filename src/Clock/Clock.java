package Clock;

import java.util.Observable;
/**
 * Clock that run time and change state for Cheap Clock
 * @author Chinatip Vichian
 *
 */
public class Clock extends Observable{
	/** boolean for checking whether alarm is on  **/
	boolean alarmOn = false;
	/** Integer for running time **/
	int hour=0,min=0,sec=0;
	/** Interger for ringing time **/
	int ringHour = 99,ringMin=99,ringSec = 99;
	/** State for running Clock **/
	public ClockState state = new DisplayTimeState(this);
	/** Update Clock **/
	public void updateTime(){ 
		state.updateTime();
		super.setChanged();
		super.notifyObservers(this);
	}
	/** When set button is pressed **/
	public void handleSetKey(){ state.performSet(); }
	/** Set ring time that was set from Cheap Clock **/
	public void setRingTime(int hour,int min,int sec){
		this.ringHour = hour;
		this.ringMin = min;
		this.ringSec = sec;
	}
	/** Set state equals RingingState **/
	public void setRingState(){
		state = new RingingState(this);
	}
	/**
	 * Get Integer which telling CheapClock to check and blink
	 * @return integer for telling CheapClock to blink during 
	 * SetHourState, SetMinuteState, SetSecondState state
	 */
	public int blink(){
		if(state instanceof SetHourState){
			return 0;
		}
		else if (state instanceof SetMinuteState){
			return 1;
		}
		else if (state instanceof SetSecondState){
			return 2;
		}
		return 99;
	}
	/**
	 * Check that is alarm on or not
	 * @return 1) true if alarm is on.
	 * 2) false if alarm is off.
	 */
	public boolean alarmIsOn(){
		return alarmOn;
	}
}
