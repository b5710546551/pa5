package Clock;

/**
 * SetSecondState is the state that CheapClock is during setting alarm time for second.
 * @author Chinatip Vichian
 *
 */
public class SetSecondState extends ClockState{

	public SetSecondState(Clock clock) {
		super(clock);
		// TODO Auto-generated constructor stub
	}
	/** handle "set" key press */
	@Override
	public void performSet() {
		clock.setRingTime(clock.hour, clock.min, clock.sec);
		clock.state = new DisplayTimeState(clock);
		clock.alarmOn = true;
	}
	/** handle updateTime event notification */
	@Override
	public void updateTime() {
		
	}
	/** Call this method when plus button is pressed **/
	@Override
	public void plus() {
		if(clock.sec>=0 && clock.sec<59) clock.sec++;
		else clock.sec = 0;
		clock.updateTime();
	}
	/** Call this method when minus button is pressed **/
	@Override
	public void minus() {
		if(clock.sec>0) clock.sec--;
		else clock.sec = 59;
		clock.updateTime();
	}

}
