
import java.util.TimerTask;


public class ClockTask extends TimerTask{
	private Clock clock;
	public ClockTask(Clock clock){
		this.clock = clock;
	}
	@Override
	public void run() {
		clock.updateTime();
	}
	

}
