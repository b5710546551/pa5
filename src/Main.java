import java.util.Timer;
import java.util.TimerTask;
import Clock.CheapClock;
import Clock.Clock;
import Clock.ClockTask;
/**
 * Main class that help run Cheap Clock UI
 * @author Chinatip Vichian
 *
 */
public class Main {
	/** interval that help run timer **/
	final static long INTERVAL = 500;
	/** Main method that run UI **/
	public static void main(String [] args){
		Clock clock = new Clock();
		TimerTask clockTask = new ClockTask(clock);
		Timer timer = new Timer();
		CheapClock cheapclock = new CheapClock(clock);
		long delay = 1000 - System.currentTimeMillis()%1000;
		timer.scheduleAtFixedRate( clockTask, delay, INTERVAL );
		clock.addObserver(cheapclock);
	}
}
